/* -------------- CRUD Operations ------------------*/

/* Insert Documents (CREATE)

		Syntax:

			Inserting One Document
				- db.collectionName.insertOne({
						"fieldA": "valueA",
						"fieldB": "valueB"
				})

			Inserting Many Documents
				- db.collectionName.insertMany([
						{
							"fieldA": "valueA",
							"fieldB": "valueB"
						}
				])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilA@mail.com",
			"department": "none"
		}
	])


/* ------------ Mini Activity --------------- */

/*
		1. Make a new collection with the name courses
		2. Insert the following fields and values

				name: JavaScript 101
				prce: 5000
				description: Introduction to JavaScript
				isActive: true

				name: HTML 101
				prce: 2000
				description: Introduction to HTML
				isActive: true

				name: CSS 101
				prce: 2500
				description: Introduction to CSS
				isActive: false
*/

db.courses.insertMany([
		{
				"name": "JavaScript 101",
				"price": 5000,
				"description": "Introduction to JavaScript",
				"isActive": true
		},
		{
				"name": "HTML 101",
				"price": 2000,
				"description": "Introduction to HTML",
				"isActive": true
		},
		{
				"name": "CSS 101",
				"price": 2500,
				"description": "Introduction to CSS",
				"isActive": true
		}
	])


/* Find Documents (READ)

		Syntax:
			- db.collectionName.find() -- retrieves all documents in the collection
			- db.collectionName.find({"criteria": "value"}) -- retrieves all documents that match the criteria
			- db.collectionName.findOne({"criteria": "value"}) -- retrieves the first document that matched the criteria
			- db.collectionName.findOne() -- retrieves the first document in the collection

*/

db.users.find()
db.users.find({"firstName": "Jane"})
db.users.find({"lastName": "Armstrong", "age": 82})


/* Updating Documents (UPDATE)

		Updating One Document

			Syntax:
				- db.collectionName.updateOne(
						{
							"criteria": "field",
						},
						{
							$set: {
									"fieldToBeUpdated": "updatedValue"
							}
						}
					)

		Updating Multiple Documents

			Syntax:
				- db.collectionName.updateMany(
							{
								"criteria": "field",
							},
							{
								$set: {
										"fieldToBeUpdated": "updatedValue"
								}
							}
						)

		Removing a field

			Syntax:
				- db.collectionName.updateOne(
						{
							"criteria": "field",
						},
						{
							$unset: {
									"fieldToBeUpdated": "updatedValue"
							}
						}
					)
*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Account",
	"age": 0,
	"department": "none"
})

db.users.updateOne(
		{
			"firstName": "Test"
		},
		{
			$set: {
					"firstName": "Bill",
					"lastName": "Gates",
					"age": 65,
					"email": "billg@gmail.com",
					"department": "operations",
					"isActive": true
			}
		}
	)

db.users.updateMany(
		{
			"department": "none"
		},
		{
			$set: {
					"department": "HR"
			}
		}
	)

db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$unset: {
					"status": "active"
			}
		}
	)


/* ------------ Mini Activity --------------- */

/* 
	1. Update the HTML 101 Course
			- Make the isActive to false
	2. Add enrolledd field to all the documents in our courses collection
			Enrolless: 10

*/

db.courses.updateOne(
	{"name": "HTML 101"},
	{$set: {"isActive": false}}
)

db.courses.updateMany(
		{}, // all fields
		{
			$set : {
				"enrolless": 10
			}
		}
	)


/* Deleting Documents (DELETE)

		Deleting a single document

			Syntax:
				- db.collectionName.deleteOne({}) -- deletes the first document
				- db.collectionName.deleteOne({"criteria": "value"}) -- deletes the document that matched the criteria

		Deleting multiple documents

			Syntax:
				- db.collectionName.deleteMany({"criteria": "value"})
				- db.collectionName.deleteMany({}) -- deletes all the documents
*/

// document to delete
db.users.insertOne({
	"firstName": "test"
})

db.users.deleteOne({"firstName": "test"})

db.users.deleteMany(
		{
			"department": "HR"
		}
	)

db.courses.deleteMany({})